import 'package:flutter/material.dart';

// Styles
final double borderMargin = 24.0;

final TextStyle fadedTextStyle = TextStyle(
  fontSize: 16.0,
  fontWeight: FontWeight.bold,
  color: Color(0x99FFFFFF),
);

final TextStyle whiteHeadingTextStyle = TextStyle(
  fontSize: 40.0,
  fontWeight: FontWeight.bold,
  color: Color(0xFFFFFFFF),
);

final TextStyle categoryTextStyle = TextStyle(
  fontSize: 14.0,
  fontWeight: FontWeight.bold,
  color: Color(0xFFFFFFFF),
);

final TextStyle filterLable = TextStyle(
  fontSize: 14.0,
  fontWeight: FontWeight.bold,
  color: Colors.black,
);

final TextStyle selectedCategoryTextStyle = categoryTextStyle.copyWith(
  color: Color(0xFFFF4700),
);

final TextStyle activitieTitleTextStyle = TextStyle(
  fontSize: 20.0,
  fontWeight: FontWeight.bold,
  color: Color(0xFF000000),
);

final TextStyle activitieWhiteTitleTextStyle = TextStyle(
  fontSize: 38.0,
  fontWeight: FontWeight.bold,
  color: Color(0xFFFFFFFF),
);

final TextStyle activitieCategorieTextStyle = TextStyle(
  fontSize: 20.0,
);

final TextStyle activitieLocationTextStyle = TextStyle(
  fontSize: 15.0,
  color: Color(0xFF000000),
);

final TextStyle columnDetailsActivityLable = TextStyle(
  fontSize: 15.0,
  color: Colors.white,
  fontWeight: FontWeight.w800,
);

final TextStyle guestTextStyle = TextStyle(
  fontSize: 16.0,
  fontWeight: FontWeight.w800,
  color: Color(0xFF000000)
);

final TextStyle punchLine1TextStyle = TextStyle(
  fontSize: 28.0,
  fontWeight: FontWeight.w800,
  color: Color(0xFFFF4700),
);

final TextStyle punchLine2TextStyle = TextStyle(
  fontSize: 20.0,
  fontWeight: FontWeight.w800,
  color: Color(0xFF000000),
);


final buttonTextColor = Colors.white;
final buttonBackgroundColor = Colors.white;
