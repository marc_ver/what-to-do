import 'package:flutter/material.dart';
import 'package:what_to_do/model/activity.dart';

class AppState extends ChangeNotifier {

  String selectedCategoryId = "all";
  Activity activityFilter = new Activity(categories: "all");

  void updateCategoryId(String selectedCategoryId) {
    this.selectedCategoryId  = selectedCategoryId;
    notifyListeners();
  }

  void updateFilter(Activity activityFilter) {
    this.activityFilter  = activityFilter;
    notifyListeners();
  }
}