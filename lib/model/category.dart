import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Category {
  final String categoryId;
  final String name;
  final IconData icon;

  Category({this.categoryId, this.name, this.icon});
}

final categoryAll = Category(
  categoryId: "all",
  name: "categoryAll",
  icon: Icons.search,
);

final categoryEducation = Category(
  categoryId: "education",
  name: "categoryEducation",
  icon: Icons.school,
);

final categoryRecreational = Category(
  categoryId: "recreational",
  name: "categoryRecreational",
  icon: Icons.golf_course,
);

final categorySocial = Category(
  categoryId: "social",
  name: "categorySocial",
  icon: Icons.people,
);

final categoryDiy = Category(
  categoryId: "diy",
  name: "categoryDiy",
  icon: Icons.build,
);

final categoryCooking = Category(
  categoryId: "cooking",
  name: "categoryCooking",
  icon: Icons.cake,
);

final categoryRelaxation = Category(
  categoryId: "relaxation",
  name: "categoryRelaxation",
  icon: Icons.airline_seat_recline_extra,
);

final categoryMusic = Category(
  categoryId: "music",
  name: "categoryMusic",
  icon: Icons.music_note,
);

final categoryBusywork = Category(
  categoryId: "busywork",
  name: "categoryBusywork",
  icon: Icons.business_center
);

final categoryCharity = Category(
  categoryId: "charity",
  name: "categoryCharity",
  icon: Icons.business_center
);

final categories = [
  categoryAll,
  categoryRecreational,
  categorySocial,
  categoryCooking,
  categoryMusic,
  categoryEducation,
  categoryDiy,
  categoryRelaxation,
  categoryBusywork,
  categoryCharity
];
