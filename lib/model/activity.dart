import 'dart:ffi';

class Activity {
  String key, title, description, location, link, categories, imagepath;
  int participants;
  double accessibility, price ;

  Activity( 
      {this.key,
      this.title,
      this.description,
      this.participants,
      this.accessibility,
      this.price,
      this.location,
      this.link,
      this.imagepath,
      this.categories});

  factory Activity.fromJson(dynamic json) {
    String imagepath = "assets/event_images/";    
    switch(json['type']) { 
      case "education": {  imagepath += "education.jpg"; } 
      break; 
      case "recreational": { imagepath += "recreational.jpeg"; } 
      break; 
      case "social": { imagepath += "social.jpg"; } 
      break; 
      case "diy": { imagepath += "diy.jpg"; } 
      break; 
      case "charity": { imagepath += "charity.jpg"; } 
      break; 
      case "cooking": {  imagepath += "cooking.jpeg"; } 
      break; 
      case "relaxation": {  imagepath += "relaxation.jpg"; } 
      break; 
      case "music": {  imagepath += "music_concert.jpeg"; } 
      break; 
      case "busywork": {  imagepath += "busywork.jpeg"; } 
      break; 
      default: { imagepath += "social.jpg";} 
      break; 
   }

    return Activity(
      key: json['key'],
      title: json['activity'],
      description: json['description'] ?? "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi. ",
      participants: json['participants'],
      accessibility: json['accessibility'].toDouble(),
      price: json['price'].toDouble(),
      location: json['location'] ?? "Kastanjelaan 400, 5616 LZ Eindhoven",
      link: json['link'],
      imagepath: imagepath,
      categories: json['type'],
    );
  }
}