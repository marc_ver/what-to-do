import 'package:flutter/material.dart';
import 'package:what_to_do/model/activity.dart';
import 'package:provider/provider.dart';
import 'package:what_to_do/shared/contents/style.dart';

class ActivityDetailsHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final activity = Provider.of<Activity>(context);

    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
              color: Colors.black87,
              image: DecorationImage(
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
                image: AssetImage(
                  activity.imagepath,
                ),
              ),
            ),
          height: 350.0,
        ),
        Container(
            alignment: Alignment.center,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 100,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.2),
                  child: Text(
                    activity.title,
                    style: activitieWhiteTitleTextStyle,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.24),
                  child: FittedBox(
                    child: Row(
                      children: <Widget>[
                        Text(
                          "-",
                          style: activitieLocationTextStyle.copyWith(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Icon(
                          Icons.location_on,
                          color: Colors.white,
                          size: 15,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          activity.location,
                          style: activitieLocationTextStyle.copyWith(color: Colors.white, fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                  ),
                ),
              ]
            ),
        ),
      ],
    );
  }
}
