import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:what_to_do/helpers/app_locatizations.dart';
import 'package:what_to_do/model/activity.dart';
import 'package:what_to_do/shared/contents/style.dart';
import 'package:what_to_do/shared/extensions/string.dart';

class ActivityDetailsBlock extends StatelessWidget {

  final String title;
  final Widget rating;

  const ActivityDetailsBlock({Key key, this.title, this.rating}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return Container(
      width: screenWidth * 0.43,
      child: Padding(
        padding: EdgeInsets.all(5),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Theme.of(context).primaryColor,
          ),
          child: Padding(
            padding: EdgeInsets.all(30),
            child: Column(
              children: <Widget>[
                rating,
                Text(
                  title.capitalize(),
                  style: columnDetailsActivityLable,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
