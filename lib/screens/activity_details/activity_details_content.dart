import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:what_to_do/helpers/app_locatizations.dart';
import 'package:what_to_do/shared/extensions/string.dart';
import 'package:what_to_do/model/activity.dart';
import 'package:what_to_do/shared/contents/style.dart';

import 'activity_details_blocks.dart';

class ActivityDetailsContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Activity activity = Provider.of<Activity>(context);
    final int itemCount = 1;
    final double itemSize = 30.0;
    final Color iconColor = Colors.amber;

    final accessibility = RatingBarIndicator(
      rating: activity.accessibility,
      itemBuilder: (context, index) => FaIcon(
        FontAwesomeIcons.heart,
        color: iconColor,
        ),
      itemCount: itemCount,
      itemSize: itemSize,
      direction: Axis.horizontal,
    );

    final participants = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget> [
        Icon(
          Icons.person,
          size: itemSize,
          color: iconColor,
        ),
        Text(
          activity.participants.toString(),
          style: TextStyle(
            fontSize: itemSize - 5,
            color: Colors.white
          )
        )
      ]
    );

    final price = RatingBarIndicator(
      rating: activity.price,
      itemBuilder: (context, index) => FaIcon(
        FontAwesomeIcons.dollarSign,
        color: iconColor,
        ),
      itemCount: itemCount,
      itemSize: itemSize,
      direction: Axis.horizontal,
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(activity.title, style: punchLine1TextStyle),  
              Text(activity.description, style: activitieLocationTextStyle),
              SizedBox(
                height: 25,
              ),
              Text(
                AppLocalizations.of(context).translate('detailsextrainformation'),
                style: punchLine2TextStyle,
              ),
              Wrap(
                children: <Widget>[
                  ActivityDetailsBlock(rating: accessibility, title: AppLocalizations.of(context).translate('accessibility')),
                  ActivityDetailsBlock(rating: participants, title: AppLocalizations.of(context).translate('participants')),
                  ActivityDetailsBlock(rating: price, title: AppLocalizations.of(context).translate('price')),
                  
                  // Expanded(
                  //   flex: 5,
                  //   child: Container(
                  //     child: RatingBarIndicator(
                  //       rating: activity.price,
                  //       itemBuilder: (context, index) => FaIcon(
                  //         FontAwesomeIcons.dollarSign,
                  //         color: Colors.amber,
                  //         ),
                  //       itemCount: 1,
                  //       itemSize: 25.0,
                  //       direction: Axis.horizontal,
                  //     ),
                  //     // Text(
                  //     //   (activity.price != 0.0) ? "€" +  activity.price.toString() : AppLocalizations.of(context).translate('free'),
                  //     //   textAlign: TextAlign.right,
                  //     //   style: activitieLocationTextStyle.copyWith(
                  //     //     fontWeight: FontWeight.w900,
                  //     //     color: Theme.of(context).primaryColor,
                  //     //     fontSize: 20
                  //     //   ),
                  //   ),
                  // ),  
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
