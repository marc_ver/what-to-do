import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:what_to_do/model/activity.dart';

import 'package:what_to_do/model/activity.dart';
import 'activity_details_header.dart';
import 'activity_details_content.dart';

class ActivityDetailsPage extends StatelessWidget {

  final Activity activity;

  const ActivityDetailsPage({Key key, this.activity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Provider<Activity>.value(
          value: activity,
          child: Column(
            children: <Widget>[
              ActivityDetailsHeader(),
              ActivityDetailsContent(),
            ],
          ),
        ),
      ),
    );
  }
}
