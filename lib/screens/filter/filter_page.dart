import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:what_to_do/app_state.dart';
import 'package:what_to_do/helpers/app_locatizations.dart';
import 'package:what_to_do/model/activity.dart';
import 'package:what_to_do/model/category.dart';
import 'package:what_to_do/screens/activity_details/activity_details_page.dart';
import 'package:what_to_do/screens/home/activity_widget.dart';
import 'package:what_to_do/screens/home/home_page_background.dart';
import 'package:what_to_do/services/activityservice.dart';
import 'package:what_to_do/shared/contents/style.dart';
import 'package:what_to_do/shared/widgets/loading.dart';
import 'package:what_to_do/shared/extensions/string.dart';
import 'filter_widget.dart';



class FilterPage extends StatefulWidget {
  @override
  _FilterPage createState() => _FilterPage();
}

class _FilterPage extends State<FilterPage> {
  ActivityService activityService = new ActivityService();
  Future _future;
  String categorie = "all";
  final _formKey = GlobalKey<FormState>();
  
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
      ),
      body: ChangeNotifierProvider<AppState>(
        create: (_) => AppState(),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              HomePageBackground(
                screenHeight: MediaQuery.of(context).size.height + 15.0,
              ),
              SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(borderMargin, 20.0, borderMargin, 0),
                      child: Text(
                        AppLocalizations.of(context).translate('filters').capitalize(),
                        style: whiteHeadingTextStyle,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: borderMargin),
                      child: Consumer<AppState>(
                        builder: (context, appState, _) => FilterWidget()
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      child: Consumer<AppState>(
                        builder: (context, appState, _) => FutureBuilder(
                          future: activityService.getFilterdActivities(appState.activityFilter),
                          builder: (context, snapshot) {
                            // operation for completed state
                            if (snapshot.hasData) {
                              return ListView.builder(
                                itemCount: snapshot.data.length,
                                shrinkWrap: true,
                                physics:  ClampingScrollPhysics(),
                                itemBuilder: (context, index) {
                                  dynamic activity = snapshot.data[index];
                                  if (activity is String) {
                                    return Center(
                                      child: Text(AppLocalizations.of(context).translate('nodatafound').capitalize())
                                    );
                                  }
                                  return GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) => ActivityDetailsPage(activity: activity),
                                        ),
                                      );
                                    },
                                    child: ActivityWidget(
                                      activity: activity,
                                    ),
                                  );
                                }
                              );
                            }
                            return Loading();
                          }
                        ),
                      )
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  } 
}