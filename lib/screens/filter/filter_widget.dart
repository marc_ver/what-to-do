import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:what_to_do/app_state.dart';
import 'package:what_to_do/helpers/app_locatizations.dart';
import 'package:what_to_do/model/activity.dart';
import 'package:what_to_do/model/category.dart';
import 'package:what_to_do/shared/contents/style.dart';
import 'package:provider/provider.dart';
import 'package:what_to_do/shared/extensions/string.dart';

class FilterWidget extends StatefulWidget {
  @override
  _FilterWidget createState() => _FilterWidget();
}

class _FilterWidget extends State<FilterWidget> {
  Activity activity = new Activity();

  @override
  Widget build(BuildContext context) {
    final appState = Provider.of<AppState>(context);
    
    
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: borderMargin),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget> [
            SizedBox(height: 10,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: borderMargin),
              child: Text(
                AppLocalizations.of(context).translate('accessibility').capitalize(),
                style: filterLable,
              ),
            ),
            Slider(  
              value: activity.accessibility ?? 0.0,  
              min: 0.0,  
              max: 1.0,  
              divisions: 10,  
              activeColor: Theme.of(context).primaryColor,  
              inactiveColor: Colors.white70,  
              label: activity.accessibility.toString(),  
              onChanged: (double value) {  
                setState(() {  
                  activity.accessibility = value;  
                });  
              },  
            ), 
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: borderMargin),
              child: Text(
                AppLocalizations.of(context).translate('price').capitalize(),
                style: filterLable,
              ),
            ),
            Slider(  
              value: activity.price ?? 0.0,  
              min: 0.0,  
              max: 1.0,  
              divisions: 10,  
              activeColor: Theme.of(context).primaryColor,  
              inactiveColor: Colors.white70,  
              label: activity.price.toString(),  
              onChanged: (double value) {  
                setState(() {  
                  activity.price = value;  
                });  
              },   
            ),
            SizedBox(height: 10,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: borderMargin),
              child: Text(
                AppLocalizations.of(context).translate('participants').capitalize(),
                style: filterLable,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: borderMargin),
              child: TextField(
                decoration: InputDecoration(labelText: AppLocalizations.of(context).translate('participants').capitalize()),
                keyboardType: TextInputType.number,
                onChanged: (String value) {
                  setState(() {
                    int participants = int.parse(value);
                    activity.participants = participants;
                  });     
                }
              ), 
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: borderMargin),
              child: Text(
                AppLocalizations.of(context).translate('category').capitalize(),
                style: filterLable,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: borderMargin),
              child: DropdownButton<String>(
                value: activity.categories ?? "all",
                icon: Icon(Icons.arrow_downward),
                isExpanded: true,
                iconSize: 24,
                elevation: 16,
                style: TextStyle(color: Colors.white),
                underline: Container(
                  height: 2,
                  width: double.infinity,
                  color: Colors.black,
                ),
                onChanged: (String value) {
                  setState(() {
                    activity.categories = value;
                  });
                },
                items: categories.map<DropdownMenuItem<String>>((Category value) {
                  return DropdownMenuItem<String>(
                    value: value.categoryId,
                    child: Text(
                      value.categoryId.capitalize(),
                      style: TextStyle(color: Colors.black),
                    ),
                  );
                }).toList(),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: borderMargin),
              child: SizedBox(
                width: double.infinity,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  onPressed: () {
                    setState(() {
                      appState.updateFilter(activity);
                    });
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      AppLocalizations.of(context).translate('filter').capitalize(), 
                      style: TextStyle(fontSize: 20, color: Colors.white)
                    ),
                  ),
                ),
              ),
            ),  
            SizedBox(height: 10),
          ]
        ),
      ),
    );
  }
}
