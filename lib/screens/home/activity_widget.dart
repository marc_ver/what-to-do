import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:what_to_do/helpers/app_locatizations.dart';
import 'package:what_to_do/model/activity.dart';
import 'package:what_to_do/shared/contents/style.dart';
import 'package:what_to_do/shared/extensions/string.dart';

class ActivityWidget extends StatelessWidget {
  final Activity activity;

  const ActivityWidget({Key key, this.activity}) : super(key: key);

  @override
  Widget build(BuildContext context) {   
    String accessibility = (activity.accessibility * 10).toInt().toString();
    String priceLable = (activity.price * 10).toInt().toString();

    return Card(
      margin: const EdgeInsets.symmetric(vertical: 20),
      elevation: 4,
      color: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(24))),
      child: Padding(
        padding: const EdgeInsets.all(0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft:Radius.circular(24),
                topRight: Radius.circular(24),
              ),
              child: Image.asset(
                activity.imagepath,
                height: 200,
                fit: BoxFit.fitWidth,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                          Text(
                            activity.categories.capitalize(),
                            style: activitieCategorieTextStyle.copyWith(
                            color: Theme.of(context).primaryColor,),
                          ),
                        ]),
                        FittedBox(
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.person,
                                size: 15.0,
                              ),
                              Text(
                                activity.participants.toString(),
                                style: activitieLocationTextStyle,
                              ),
                              Text(
                                " | " + AppLocalizations.of(context).translate('accessibility') + ": ",
                                style: activitieLocationTextStyle,
                              ),
                              Text(
                                accessibility + "/10 ",
                                style: activitieLocationTextStyle,
                              ),
                              RatingBarIndicator(
                                rating: activity.accessibility,
                                itemBuilder: (context, index) => FaIcon(
                                  FontAwesomeIcons.heart,
                                  color: Colors.amber,
                                  ),
                                itemCount: 1,
                                itemSize: 20.0,
                                direction: Axis.horizontal,
                              ),
                            ],
                          ),
                        ),
                        Text(
                          activity.title,
                          style: activitieTitleTextStyle,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        RatingBarIndicator(
                            rating: activity.price,
                            itemBuilder: (context, index) => FaIcon(
                              FontAwesomeIcons.dollarSign,
                              color: Colors.amber,
                              ),
                            itemCount: 1,
                            itemSize: 25.0,
                            direction: Axis.horizontal,
                        ),
                        Text(
                          (activity.price == 0.0) ? AppLocalizations.of(context).translate('free') : priceLable + "/10",
                          textAlign: TextAlign.right,
                          style: activitieLocationTextStyle.copyWith(
                            fontWeight: FontWeight.w900,
                            color: Theme.of(context).primaryColor,
                            fontSize: 20
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
