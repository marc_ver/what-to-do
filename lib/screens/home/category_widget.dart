import 'package:flutter/material.dart';
import 'package:what_to_do/app_state.dart';
import 'package:what_to_do/helpers/app_locatizations.dart';
import 'package:what_to_do/model/category.dart';
import 'package:what_to_do/shared/contents/style.dart';
import 'package:provider/provider.dart';
import 'package:what_to_do/shared/extensions/string.dart';


class CategoryWidget extends StatelessWidget {
  final Category category;

  const CategoryWidget({Key key, this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appState = Provider.of<AppState>(context);
    final isSelected = appState.selectedCategoryId == category.categoryId;

    return Container(
        margin: const EdgeInsets.symmetric(horizontal: 8),
        width: 90,
        height: 90,
        decoration: BoxDecoration(
          border: Border.all(color: isSelected ? Colors.white : Color(0x99FFFFFF), width: 3),
          borderRadius: BorderRadius.all(Radius.circular(16)),
          color: isSelected ? Colors.white : Colors.transparent,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              category.icon,
              color: isSelected ? Theme.of(context).primaryColor : Colors.white,
              size: 40,
            ),
            SizedBox(height: 10,),
            Text(
              AppLocalizations.of(context).translate(category.name).capitalize() ?? 'default value',
              style: isSelected ? selectedCategoryTextStyle : categoryTextStyle,
            )
          ],
        ),
    );
  }
}
