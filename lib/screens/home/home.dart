import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:what_to_do/app_state.dart';
import 'package:what_to_do/helpers/app_locatizations.dart';
import 'package:what_to_do/model/activity.dart';
import 'package:what_to_do/model/category.dart';
import 'package:what_to_do/screens/activity_details/activity_details_page.dart';
import 'package:what_to_do/screens/filter/filter_page.dart';
import 'package:what_to_do/services/activityservice.dart';
import 'package:what_to_do/shared/contents/style.dart';
import 'package:what_to_do/shared/widgets/loading.dart';
import 'package:what_to_do/screens/home/home_page_background.dart';
import 'package:what_to_do/shared/extensions/string.dart';
import 'category_widget.dart';
import 'activity_widget.dart';


class Home extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {
  ActivityService activityService = new ActivityService();
  Future _future;
  String categorie = "all";

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: ChangeNotifierProvider<AppState>(
        create: (_) => AppState(),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              HomePageBackground(
                screenHeight: MediaQuery.of(context).size.height,
              ),
              SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 35, right:35, top:35, bottom:0),
                      child: Row(
                        children: <Widget>[
                          Text(
                            AppLocalizations.of(context).translate('localActivities'),
                            style: fadedTextStyle,
                          ),
                          Spacer(),
                          // Icon(
                          //   Icons.person_outline,
                          //   color: Color(0x99FFFFFF),
                          //   size: 30,
                          // ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 35.0),
                      child: Text(
                        AppLocalizations.of(context).translate('title'),
                        style: whiteHeadingTextStyle,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: borderMargin, horizontal: 16.0),
                      child: Consumer<AppState>(
                        builder: (context, appState, _) => SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: <Widget>[for (final category in categories) 
                              GestureDetector(
                              onTap: () {
                                if (categorie != category.categoryId) {
                                setState(() {
                                  // _future = activityService.getActivities(category.categoryId);
                                  categorie = category.categoryId;
                                  appState.updateCategoryId(category.categoryId);
                                  setState(() {});
                                });  
                                }
                              },
                              child: CategoryWidget(category: category),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      child: SizedBox(
                        width: double.infinity,
                        child: RaisedButton(
                          color: Colors.white,
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => FilterPage(),
                              ),
                            );
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            child: Text(
                              AppLocalizations.of(context).translate('showallfilters').capitalize(), 
                              style: TextStyle(fontSize: 20, color: Theme.of(context).primaryColor)
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      child: FutureBuilder(
                        future: activityService.getActivities(categorie),
                        builder: (context, snapshot) {
                          // operation for completed state
                          if (snapshot.hasData) {
                            return ListView.builder(
                              itemCount: snapshot.data.length,
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemBuilder: (context, index) {
                                Activity activity = snapshot.data[index];
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (context) => ActivityDetailsPage(activity: activity),
                                      ),
                                    );
                                  },
                                  child: ActivityWidget(
                                    activity: activity,
                                  ),
                                );
                              }
                            );
                          }
                          return Loading();
                        }
                      )
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  } 
}