import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart';
import 'package:http/http.dart' as http;
import 'package:what_to_do/model/activity.dart';

class ActivityService {

  final String url = 'http://www.boredapi.com/api/activity/';

  // List<Activity> parseActivities(String responseBody) {
  //   final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  //   return parsed.map<Activity>((json) => Activity.fromJson(json)).toList();
  // }

  // Future<List<Activity>> getActivities() async {
  //   try {
  //     var response = await http.get(url);
  //     print('Response status: ${response.statusCode}');
  //     print('Response body: ${response.body}');

  //     Future<List<Activity>> parseActivities = parseActivities(response.body);
      
  //     return parseActivities;
  //   }
  //   catch(e) {
  //     print(e.toString());
  //     return null;
  //   }
  // }


  Activity parseActivities(String responseBody) {
    Activity user = Activity.fromJson(jsonDecode(responseBody));
    return user;
  }

  Future<List<Activity>> getActivities(String categorie) async {
    try {
      List<Activity> activities = [];
      for(int i = 0; i < 7; i++){
        var response = (categorie == "all") ? await http.get(url) : await http.get(url + "?type=" + categorie) ;
        Activity activity = parseActivities(response.body);

        activities.add(activity);
      }
      
      return activities;
    }
    catch(e) {
      print(e.toString());
      return null;
    }
  }

  Future<List<dynamic>> getFilterdActivities(Activity filterActivity) async {
    try {
      List<Activity> activities = [];
      String category = (filterActivity.categories != "all") ? filterActivity.categories : "";
      String participants = (filterActivity.participants.toString() != "null") ? filterActivity.participants.toString() : "";
      String price = (filterActivity.price.toString() != "null") ? filterActivity.price.toString() : "";
      String accessibility = (filterActivity.accessibility.toString() != "null") ? filterActivity.accessibility.toString() : "";

      String fullUrl = url + "?type=" + category + "&participants=" + participants + "&price=" + price + "&accessibility=" + accessibility;
      for(int i = 0; i < 7; i++){
        var response = await http.get(fullUrl);
          
        Activity activity = parseActivities(response.body);

        activities.add(activity);
      }
      
      return activities;
    }
    catch(e) {
      print(e.toString());
      return ["Geen gevonden"];
    }
  }
}
