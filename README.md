# What to do

De app voor als je je verveeld. Wanneer je niets te doen hebt en je je verveeld, maar toch graag iets wilt gaan doen. Kan in deze app opzoek gaan de geschikte activiteit om te gaan doen. Kijk op de home pagina, naar verschillende activiteiten om uit te voeren. Ben je toch op zoek naar iets specifiekers ga dan naar de filter pagina en pas je zoekopdracht aan naar jouw wensen.

## Getting Started
Het project is te vinden op GitLab. Klik [hier](https://gitlab.com/marc_ver/what-to-do) om de replository te openen.

### Clone de repository
Het project kan worden gekloond worden via het volgende comando "git clone https://gitlab.com/marc_ver/what-to-do.git". Mocht het zo zijn dat er veel errors in het project zitten, omdat er bepaalde dingen niet geimporteerd zijn. Ga dan in Visual Code naar de folder en open de pubspec.yaml file. Zet hier ergens een enter in en sla het bestand weer op. Op deze manier zullen alle dependencies en imports opnieuw geladen worden.

Wanneer de repository is geklookd, kan deze geopend worden in Visual Code. Ga verolgens naar "Terminal" -> "New terminal" boven in de menu balk. Zorg ervoor dat het path in de "what-to-do" folder staat. Is dit niet het geval voer dan eerst het volgende comando uit "cd what-to-do". Als je in de juiste folder zit voer dan het volgende commando uit "flutter run".

### Run de applicatie door middel van een .apk
Wanneer je een clone hebt gemaakt van de repository, is er in de folder "what-to-do" een .apk bestand te vinden. Deze kan je vervolgens installeren op je telefoon. 

